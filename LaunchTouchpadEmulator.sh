#!/bin/sh

pidof TouchpadEmulator && killall TouchpadEmulator ||
case "$(xrandr -q --verbose | grep DSI-1 | cut -d' ' -f6)" in
	normal)
		exec TouchpadEmulator 0 ;;
	right)
		exec TouchpadEmulator 90 ;;
	inverted)
		exec TouchpadEmulator 180 ;;
	left)
		exec TouchpadEmulator 270 ;;
esac
