/*---------------------------------------------------------*\
| Touchpad Emulator                                         |
|                                                           |
|   A Virtual Mouse for Linux Phones                        |
|                                                           |
|   This program hijacks the touchscreen input device and   |
|   exposes a virtual mouse device using uinput.  The mouse |
|   is controlled by dragging a finger on the touchscreen   |
|   as if it were a laptop touchpad.                        |
|                                                           |
|   License: GNU GPL V2                                     |
|                                                           |
|   Adam Honse (CalcProgrammer1)                            |
|   calcprogrammer1@gmail.com                               |
\*---------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include <errno.h>
#include <dbus/dbus.h>
#include <dbus/dbus-glib.h>
#include <fcntl.h>
#include <linux/uinput.h>
#include <poll.h>
#include <pthread.h>
#include <unistd.h>

/*---------------------------------------------------------*\
| Event Codes                                               |
\*---------------------------------------------------------*/
#define EVENT_TYPE      EV_ABS
#define EVENT_CODE_X    ABS_X
#define EVENT_CODE_Y    ABS_Y

/*---------------------------------------------------------*\
| Global Variables                                          |
\*---------------------------------------------------------*/
int     rotation            = 0;
char    query_buf[64];

int     touchscreen_fd      = 0;
int     virtual_mouse_fd    = 0;

int     close_flag          = 0;
int     touchpad_enable     = 1;
    
/*---------------------------------------------------------*\
| emit                                                      |
|                                                           |
| Emits an input event                                      |
\*---------------------------------------------------------*/

void emit(int fd, int type, int code, int val)
{
    struct input_event ie;

    ie.type = type;
    ie.code = code;
    ie.value = val;
    ie.input_event_sec = 0;
    ie.input_event_usec = 0;

    write(fd, &ie, sizeof(ie));
}

/*---------------------------------------------------------*\
| open_uinput                                               |
|                                                           |
| Creates the virtual mouse device                          |
\*---------------------------------------------------------*/

void open_uinput(int* fd)
{
    /*-----------------------------------------------------*\
    | If virtual mouse is already opened, return            |
    \*-----------------------------------------------------*/
    if(*fd != 0)
    {
        return;
    }

    /*-----------------------------------------------------*\
    | Open the uinput device                                |
    \*-----------------------------------------------------*/
    *fd = open("/dev/uinput", O_WRONLY | O_NONBLOCK);

    /*-----------------------------------------------------*\
    | Virtual mouse provides left and right keys; x, y, and |
    | wheel axes and has direct property                    |
    \*-----------------------------------------------------*/
    ioctl(*fd, UI_SET_EVBIT,  EV_KEY);
    ioctl(*fd, UI_SET_KEYBIT, BTN_LEFT);
    ioctl(*fd, UI_SET_KEYBIT, BTN_RIGHT);

    ioctl(*fd, UI_SET_EVBIT,  EV_REL);
    ioctl(*fd, UI_SET_RELBIT, REL_X);
    ioctl(*fd, UI_SET_RELBIT, REL_Y);
    ioctl(*fd, UI_SET_RELBIT, REL_WHEEL);

    ioctl(*fd, UI_SET_PROPBIT, INPUT_PROP_DIRECT);

    /*-----------------------------------------------------*\
    | Set up virtual mouse device.  Use fake USB ID and name|
    | it "Touchpad Emulator"                                |
    \*-----------------------------------------------------*/
    struct uinput_setup usetup;

    memset(&usetup, 0, sizeof(usetup));

    usetup.id.bustype = BUS_USB;
    usetup.id.vendor  = 0x1234;
    usetup.id.product = 0x5678;
    strcpy(usetup.name, "Touchpad Emulator");

    ioctl(*fd, UI_DEV_SETUP, &usetup);

    /*-----------------------------------------------------*\
    | Create the virtual mouse.  The cursor should appear   |
    | on screen after this call.                            |
    \*-----------------------------------------------------*/
    ioctl(*fd, UI_DEV_CREATE);
}

/*---------------------------------------------------------*\
| close_uinput                                              |
|                                                           |
| Closes the virtual mouse device                           |
\*---------------------------------------------------------*/

void close_uinput(int* fd)
{
    /*-----------------------------------------------------*\
    | Destroy the virtual mouse.  The cursor should         |
    | disappear from the screen after this call if no other |
    | mice are present.                                     |
    \*-----------------------------------------------------*/
    ioctl(*fd, UI_DEV_DESTROY);
    close(*fd);

    *fd = 0;
}

void scan_and_open_devices(char* touchscreen_device)
{
    int     event_id        = 0;
    int     touchscreen_id  = -1;

    while(touchscreen_id == -1)
    {
        /*-------------------------------------------------*\
        | Create the input event name path                  |
        \*-------------------------------------------------*/
        char input_dev_buf[1024];

        snprintf(input_dev_buf, 1024, "/sys/class/input/event%d/device/name", event_id);

        /*-------------------------------------------------*\
        | Open the input event path to get the name         |
        \*-------------------------------------------------*/
        int input_name_fd = open(input_dev_buf, O_RDONLY|O_NONBLOCK);

        if(input_name_fd < 0)
        {
            break;
        }

        memset(input_dev_buf, 0, 1024);
        read(input_name_fd, input_dev_buf, 1024);
        close(input_name_fd);

        printf("Input Device %d: %s", event_id, input_dev_buf);

        /*-------------------------------------------------*\
        | Check if this input is the touchscreen            |
        \*-------------------------------------------------*/
        if(strncmp(input_dev_buf, touchscreen_device, strlen(touchscreen_device)) == 0)
        {
            touchscreen_id = event_id;
        }
        
        /*-------------------------------------------------*\
        | Move on to the next event                         |
        \*-------------------------------------------------*/
        event_id++;
    }

    if(touchscreen_id == -1)
    {
        printf("Did not find matching input devices, exiting.\r\n");
        exit(1);
    }

    /*-----------------------------------------------------*\
    | Open the touchscreen device                           |
    \*-----------------------------------------------------*/
    char touchscreen_dev_path[1024];

    snprintf(touchscreen_dev_path, 1024, "/dev/input/event%d", touchscreen_id);

    touchscreen_fd = open(touchscreen_dev_path, O_RDONLY|O_NONBLOCK);
}

/*---------------------------------------------------------*\
| main                                                      |
|                                                           |
| Main function                                             |
\*---------------------------------------------------------*/

int main(int argc, char* argv[])
{

    if(argc == 2) rotation = atoi(argv[1]);

    /*-----------------------------------------------------*\
    | Open touchscreen device by name                       |
    \*-----------------------------------------------------*/
    scan_and_open_devices("Goodix Capacitive TouchScreen");

    /*-----------------------------------------------------*\
    | Open the virtual mouse                                |
    \*-----------------------------------------------------*/

    open_uinput(&virtual_mouse_fd);
    
    sleep(1);

    /*-----------------------------------------------------*\
    | Open the touchscreen device and grab exclusive access |
    \*-----------------------------------------------------*/
    ioctl(touchscreen_fd, EVIOCGRAB, 1);

    int max_x[6];
    int max_y[6];

    ioctl(touchscreen_fd, EVIOCGABS(ABS_MT_POSITION_X), max_x);
    ioctl(touchscreen_fd, EVIOCGABS(ABS_MT_POSITION_Y), max_y);

    printf("Touchscreen Max X:%d, Max y:%d\r\n", max_x[2], max_y[2]);

    /*-----------------------------------------------------*\
    | Initialize virtual mouse pointer tracking variables   |
    \*-----------------------------------------------------*/
    int prev_x              = 0;
    int prev_y              = 0;
    int prev_wheel_x        = 0;
    int prev_wheel_y        = 0;

    int init_prev_x         = 0;
    int init_prev_y         = 0;
    int init_prev_wheel_x   = 0;
    int init_prev_wheel_y   = 0;
    
    int fingers             = 0;
    int dragging            = 0;

    /*-----------------------------------------------------*\
    | Initialize time tracking variables                    |
    \*-----------------------------------------------------*/
    struct timeval time_active;
    struct timeval time_release;
    struct timeval two_finger_time_active;

    /*-----------------------------------------------------*\
    | Initialize flag variables                             |
    \*-----------------------------------------------------*/
    close_flag              = 0;
    touchpad_enable         = 1;
    
    /*-----------------------------------------------------*\
    | Set up file descriptor polling structures             |
    \*-----------------------------------------------------*/
    struct pollfd fds[2];
    
    fds[0].fd               = touchscreen_fd;
    
    fds[0].events           = POLLIN;
    fds[1].events           = POLLIN;
    
    /*-----------------------------------------------------*\
    | Main loop                                             |
    \*-----------------------------------------------------*/
    while(!close_flag)
    {
        /*-------------------------------------------------*\
        | Poll until an input event occurs                  |
        \*-------------------------------------------------*/
        int ret = poll(fds, 2, 5000);
        
        if(ret <= 0) continue;
        
        /*-------------------------------------------------*\
        | Read the touchscreen event                        |
        \*-------------------------------------------------*/
        struct input_event touchscreen_event;

        ret = read(touchscreen_fd, &touchscreen_event, sizeof(touchscreen_event));

        if(ret > 0 && touchpad_enable)
        {
            if(touchscreen_event.type == EV_KEY && touchscreen_event.value == 1 && touchscreen_event.code == BTN_TOUCH)
            {
                struct timeval cur_time;
                cur_time.tv_sec = touchscreen_event.input_event_sec;
                cur_time.tv_usec = touchscreen_event.input_event_usec;
                time_active = cur_time;
                struct timeval ret_time;
                timersub(&cur_time, &time_release, &ret_time);
                if(ret_time.tv_sec == 0 && ret_time.tv_usec < 150000)
                {
                    //printf("drag started\r\n");
                    dragging = 1;
                    emit(virtual_mouse_fd, EV_KEY, BTN_LEFT,   1);
                    emit(virtual_mouse_fd, EV_SYN, SYN_REPORT, 0);
                }
                init_prev_x = 1;
                init_prev_y = 1;
                //printf("key press\r\n");
            }
            if(touchscreen_event.type == EV_KEY && touchscreen_event.value == 0 && touchscreen_event.code == BTN_TOUCH)
            {
                struct timeval cur_time;
                cur_time.tv_sec = touchscreen_event.input_event_sec;
                cur_time.tv_usec = touchscreen_event.input_event_usec;
                time_release = cur_time;
                //printf("key release\r\n");
                struct timeval ret_time;
                timersub(&cur_time, &time_active, &ret_time);
                if(ret_time.tv_sec == 0 && ret_time.tv_usec < 150000)
                {
                    //printf("click\r\n");
                    emit(virtual_mouse_fd, EV_KEY, BTN_LEFT,   1);
                    emit(virtual_mouse_fd, EV_SYN, SYN_REPORT, 0);
                    emit(virtual_mouse_fd, EV_KEY, BTN_LEFT,   0);
                }

                if(dragging)
                {
                    //printf("drag stopped\r\n");
                    emit(virtual_mouse_fd, EV_KEY, BTN_LEFT, 0);
                    dragging = 0;
                }
            }
            if(touchscreen_event.type == EV_ABS && touchscreen_event.code == ABS_MT_TRACKING_ID && touchscreen_event.value >= 0)
            {
                fingers++;

                if(fingers == 2)
                {
                    two_finger_time_active.tv_sec = touchscreen_event.input_event_sec;
                    two_finger_time_active.tv_usec = touchscreen_event.input_event_usec;
                    init_prev_wheel_x = 1;
                    init_prev_wheel_y = 1;
                }

                //printf("finger pressed, %d fingers on screen\r\n", fingers);
            }
            if(touchscreen_event.type == EV_ABS && touchscreen_event.code == ABS_MT_TRACKING_ID && touchscreen_event.value == -1)
            {
                if(fingers == 2)
                {
                    struct timeval cur_time;
                    cur_time.tv_sec = touchscreen_event.input_event_sec;
                    cur_time.tv_usec = touchscreen_event.input_event_usec;
                    struct timeval ret_time;
                    timersub(&cur_time, &two_finger_time_active, &ret_time);

                    if(ret_time.tv_sec == 0 && ret_time.tv_usec < 150000)
                    {
                        //printf("right click\r\n");
                        emit(virtual_mouse_fd, EV_KEY, BTN_RIGHT,  1);
                        emit(virtual_mouse_fd, EV_SYN, SYN_REPORT, 0);
                        emit(virtual_mouse_fd, EV_KEY, BTN_RIGHT,  0);
                    }
                    
                    init_prev_x = 1;
                    init_prev_y = 1;
                }

                fingers--;

                if(fingers < 0)
                {
                    fingers = 0;
                }

                //printf("finger released, %d fingers on screen\r\n", fingers);
            }
            if(touchscreen_event.type == EVENT_TYPE && touchscreen_event.code == EVENT_CODE_X)
            {
                if(rotation == 90 || rotation == 180)
                {
                    touchscreen_event.value = max_x[2] - touchscreen_event.value;
                }
                
                if(fingers == 1)
                {
                    if(!init_prev_x)
                    {
                        if(rotation == 0 || rotation == 180)
                        {
                            emit(virtual_mouse_fd, EV_REL, REL_X, touchscreen_event.value - prev_x);
                        }
                        else if(rotation == 90 || rotation == 270)
                        {
                            emit(virtual_mouse_fd, EV_REL, REL_Y, touchscreen_event.value - prev_x);
                        }
                    }
                        
                    prev_x = touchscreen_event.value;
                    init_prev_x = 0;
                }
                else if(fingers == 2)
                {
                    if(init_prev_wheel_x)
                    {
                        prev_wheel_x = touchscreen_event.value;
                        init_prev_wheel_x = 0;
                    }
                    else
                    {
                        if(rotation == 90 || rotation == 270)
                        {
                            int accumulator_wheel_x = touchscreen_event.value;
                            
                            if(abs(accumulator_wheel_x - prev_wheel_x) > 15)
                            {
                                emit(virtual_mouse_fd, EV_REL, REL_WHEEL, (accumulator_wheel_x - prev_wheel_x) / 10);
                                prev_wheel_x = accumulator_wheel_x;
                            }
                        }
                    }
                }    

            }
            if(touchscreen_event.type == EVENT_TYPE && touchscreen_event.code == EVENT_CODE_Y)
            {
                if(rotation == 180 || rotation == 270)
                {
                    touchscreen_event.value = max_y[2] - touchscreen_event.value;
                }

                if(fingers == 1)
                {
                    if(!init_prev_y)
                    {
                        if(rotation == 0 || rotation == 180)
                        {
                            emit(virtual_mouse_fd, EV_REL, REL_Y, touchscreen_event.value - prev_y);
                        }
                        else if(rotation == 90 || rotation == 270)
                        {
                            emit(virtual_mouse_fd, EV_REL, REL_X, touchscreen_event.value - prev_y);
                        }
                    }
    
                    prev_y = touchscreen_event.value;
                    init_prev_y = 0;
                }
                else if(fingers == 2)
                {
                    if(init_prev_wheel_y)
                    {
                        prev_wheel_y = touchscreen_event.value;
                        init_prev_wheel_y = 0;
                    }
                    else
                    {
                        if(rotation == 0 || rotation == 180)
                        {
                            int accumulator_wheel_y = touchscreen_event.value;
                            
                            if(abs(accumulator_wheel_y - prev_wheel_y) > 15)
                            {
                                emit(virtual_mouse_fd, EV_REL, REL_WHEEL, (accumulator_wheel_y - prev_wheel_y) / 10);
                                prev_wheel_y = accumulator_wheel_y;
                            }
                        }
                    }
                }
                    
            }
            if(touchscreen_event.type == EV_SYN && touchscreen_event.code == SYN_REPORT)
            {
                emit(virtual_mouse_fd, EV_SYN, SYN_REPORT, 0);
            }
        }
    }

    sleep(1);

    /*-----------------------------------------------------*\
    | Close the virtual mouse                               |
    \*-----------------------------------------------------*/
    close_uinput(&virtual_mouse_fd);
    
    return 0;
}
