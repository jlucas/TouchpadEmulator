# Touchpad Emulator

This fork is meant to be used in my personal sxmo-like environment.

## Dependencies:

* Alpine/PostmarketOS - `sudo apk add build-base linux-headers`

* Debian/Mobian - `sudo apt install build-essential linux-headers`

## Compiling:

* `make`

## Input Events:

* Touchpad Emulator now uses input event names to automatically determine the event IDs.  The event names are currently hard-coded to be PinePhone-specific, but I plan to move this to a config file soon.
  * Touchscreen: "Goodix Capacitive TouchScreen"

## Running:

* `sh LaunchTouchpadEmulator.sh`

## Installing:

* `sudo make install`

## Uninstalling:

* `sudo make uninstall`

## Controls:
* Moving one finger on touchscreen emulates mouse movement
* Tapping one finger on touchscreen emulates mouse left click
* Tap-and-hold one finger on touchscreen emulates mouse left click and drag
* Holding one finger and tapping a second finger on touchscreen emulates mouse right click
* Moving two fingers on touchscreen emulates scroll wheel (vertical axis only)
